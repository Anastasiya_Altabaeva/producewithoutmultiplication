import java.util.Scanner;

public class ProduceWithoutMultiplication {
        public static void main(String[] args) {

            Scanner scanner = new Scanner(System.in);

            System.out.println("Введите первое число");
            int a = scanner.nextInt();

            System.out.println("Введите второе число");
            int b = scanner.nextInt();

            System.out.println("Произведение чисел " + a + " и " + b + " равно " + mult(a, b));
        }
        
    /**
     * Возвращает произведение двух чисел. Для нахождения произведения не используется
     * оператор умножения.
     *
     * @param a первый множитель
     * @param b второй множитель
     * @return произведение
     */

        private static int mult(int a, int b) {
            if ((a == 0) || (b == 0)) {
                return 0;
            }
            int min;
            int max;
            if (a > b) {
                max = abs(a);
                min = abs(b);
            } else {
                max = abs(b);
                min = abs(a);
            }
            int result = max;
            while (min > 1) {
                min--;
                result += max;

            }

            if (((a < 0) && (b > 0)) || ((a > 0) && (b < 0))) {
                result = - result;

            }
            return result;
        }

        private static int abs(int a) {
            a = (a < 0) ? -a : a;
            return a;
        }

    }



